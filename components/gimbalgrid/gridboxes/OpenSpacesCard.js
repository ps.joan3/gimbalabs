import Image from "next/image";
import React from "react";

export default function OpenSpacesCard() {
  return (
    <div className="">
        <Image src="/open-spaces-card-1.png" width={1920} height={1080} />
    </div>
  );
}